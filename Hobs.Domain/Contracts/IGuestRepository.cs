﻿using Hobs.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hobs.Domain.Contracts
{
    public interface IGuestRepository
    {
        Guest Get(int id);
        IQueryable<Guest> Get();

        int Create(Guest guest);

        bool Update(Guest guest);

        bool Delete(Guest guest);
    }
}
