﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hobs.Domain.Models
{
    public class EmployeePayment
    {
        public int PaymentID { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int EmployeeID { get; set; }
        public DateTime PresentDays { get; set; }
        public decimal Salary { get; set; }
        public decimal AdvancePaid { get; set; }
        public decimal Deductions { get; set; }
        public DateTime Overtime { get; set; }
        public decimal OvertimeRate { get; set; }
        public decimal OverTimeAmount { get; set; }
        public DateTime PaymentDate { get; set; }
        public string ModeOfPayment { get; set; }
        public decimal NetPay { get; set; }
    }
}
