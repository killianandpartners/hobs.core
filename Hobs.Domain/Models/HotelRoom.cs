﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hobs.Domain.Models
{
    public class HotelRoom
    {
        public int HotelRoomID { get; set; }
        public string RoomFloor { get; set; }
        public int RoomTypeID { get; set; }
    }
}

