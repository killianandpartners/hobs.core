﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hobs.Domain.Models
{
   public class EmployeeRegistration
    {
       public int EmployeeID { get; set; }
       public string EmployeeName { get; set; }
       public string Address { get; set; }
       public string MobileNo { get; set; }
       public string Email { get; set; }
       public string Department { get; set; }
       public DateTime DateOfEmployement { get; set; }
       public decimal Salary { get; set; }
       public DateTime BasicWorkingTime { get; set; }
    }
}
