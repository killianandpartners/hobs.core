﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hobs.Domain.Models
{
    public class Reservations
    {
        public int ReservationsID { get; set; }
        public int GuestID { get; set; }
        public decimal RoomNo { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime DateOut { get; set; }
        public bool Status { get; set; }
        public string Notes { get; set; }
    }

}
