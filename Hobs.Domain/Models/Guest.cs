﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hobs.Domain.Models
{
    public class Guest
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Surname{ get; set; }
        public string PhoneNos { get; set; }
        public string Email { get; set; }
        
    }
}
